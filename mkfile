< .env
MKSHELL=mkx

help:Q:	## Show this message
	cmd/show-help mkfile

dbuild:Q:	## build all documents in this directory using docker
	cmd/mk-from-docker build

build:Q:	## build all documents
	backtick NPROC
	{ pipeline { grep proc /proc/cpuinfo } wc -l }
	pipeline {
		cmd/targets
	}
	xargs mk -k

%.t.tex:Q:	%.md	%.json
	cmd/build-doc
		-V date="${DATE}"
		--template=eisvogel
		--standalone
		-o ${target}
		${stem}.md

%.t.pdf:Q:	%.md	%.json
	cmd/build-doc
		-V date="${DATE}"
		--template=eisvogel
		-o ${target}
		${stem}.md

%.p.html:Q:	%.pmd	%.json
	cmd/build-doc
		-V date="${DATE}"
		-t slidy
		-o ${target}
		${stem}.pmd

%.p.tex:Q:	%.pmd	%.json
	cmd/build-doc
		-V date="${DATE}"
		-t beamer
		--standalone
		-o ${target}
		${stem}.pmd

%.p.pdf:Q:	%.pmd	%.json
	cmd/build-doc
		-V date="${DATE}"
		-t beamer
		-o ${target}
		${stem}.pmd

%.json:QE:
	mkvars
	ifelse {
		stest -ve ${target}
	}
	{
	tmpfile ${target}.build
	echo []
	}
	true
