---
title: #
date: #
subtitle: #
author:
- #
- # supports multiple authors
links-as-notes: true
toc: true
toc-title: Contenido
#logo: # you can add a logo
logo-width: 10cm
template: eisvogel
lang: es-MX
titlepage: true
#titlepage-background: # you can add a title background
#titlepage-rule-color: 'AAAAAA' # modify this color
#titlepage-text-color: 'BBBBBB' # modify this color
highlight-style: pygments
codeblock-font-size: \tiny
syntax-definitions:
- bash
#footer-left: #
#header-includes: #
documentation:
- https://pandoc.org/MANUAL.html
- https://github.com/Wandmalfarbe/pandoc-latex-template
---

\newpage
