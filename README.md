![Build Status](https://gitlab.com/xihh87/ppt/badges/main/pipeline.svg)

Mi plantilla para proyectos de `pandoc` usando mi [`pandoc-build-server`](https://gitlab.com/xihh87/pandoc-build-server/ ) y [`pandoc-latex-template`](https://github.com/xihh87/pandoc-latex-template ).

Requisitos
----------

-   Agregar `cmd/mkx` al PATH.

-   Instalar:

    -   [execline](https://www.skarnet.org/software/execline/)
    -   [mk](https://tools.suckless.org/9base/)
    -   [pandoc](https://pandoc.org/)
    -   [tex-live](http://tug.org/texlive/http://tug.org/texlive/)

Forma de uso
------------

Construir todos los documentos en este directorio:

```
mk build
```

Generar una presentación

```
$ NAME=
$ $EDITOR ${NAME}.pmd
$ mk ${NAME}.p.pdf
```

Generar un documento:

```
$ NAME=
$ $EDITOR ${NAME}.md
$ mk ${NAME}.t.pdf
```

Si instalas `docker` y `execline` puedes usar:

```
cmd/mk-from-docker build
```

Si subes tus fuentes a GitLab,
tus documentos se generarán automáticamente
[como artefactos en tu proyecto](https://gitlab.com/${GROUP}/${PROJECT}/-/jobs ).

Puedes usar [mi imagen de git para construir tus documentos](https://gitlab.com/xihh87/pandoc-build-server/ ):

```
cmd/mk-from-docker ${TARGETS}
mk dbuild
```

La plantilla se configura editando `.env`.
Si cambias el formato de la citas, [agrega el CSL correspondiente](https://github.com/citation-style-language/styles "El repositorio oficial de CSL.").

Cómo contribuir
---------------

Las presentaciones se nombran `00.-título.pmd`,
el número permite ordenarlas,
el título muestra de qué trata,
la extensión `.pmd` compila con beamer.

Los documentos se nombran `00.-título.md`,
el número permite ordenarlas,
el título muestra de qué trata,
la extensión `.md` compila como documento pdf.

[Las necesidades de desarrollo se documentan en los issues](https://gitlab.com/${GROUP}/${PROJECT}/-/issues)
o [pueden proponerse directamente cambios como solicitud de cambios](https://gitlab.com/${GROUP}/${PROJECT}/-/merge_requests ).


Referencias
-----------
